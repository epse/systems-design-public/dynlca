# Dynlca

The dynlca framework has been developed by members of the EPSE group in the following paper:

> [Reinert, C., Deutz, S., Minten, H., Dörpinghaus, L., von Pfingsten, S., Baumgärtner, N., & Bardow, A. (2021). Environmental impacts of the future German energy system from integrated energy systems optimization and dynamic life cycle assessment. Computers & Chemical Engineering, 153, 107406](https://doi.org/10.1016/j.compchemeng.2021.107406)

You can find the corresponding Git here: 

https://git-ce.rwth-aachen.de/ltt/dynlca
